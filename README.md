# EFI NVRAM Dump Reader
This is a Rust translation of a [small utility](https://gitlab.com/hounsell/efi-dump-reader) I put together to read the data from an NVRAM dump from an EFI shell, obtained by running the following command:

```
dmpstore -all -s <file>
```

### References
I referred to the following code and documentation when creating this tool:

* [UEFI Specification v2.7B](https://uefi.org/sites/default/files/resources/UEFI%20Spec%202_7_B.pdf) - pages 228-229
* [Tianocore / UDK 2018 - `dmpstore.c`](https://github.com/tianocore/edk2/blob/3806e1fd139775610d8f2e7541a916c3a91ad989/ShellPkg/Library/UefiShellDebug1CommandsLib/DmpStore.c) - The `LoadVariablesFromFile` function, Lines 114-281

### Licence
Copyright © 2021, Thomas Hounsell.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.