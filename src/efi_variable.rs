#[path = "efi_variable_attributes.rs"] pub mod efi_variable_attributes;

use byteorder::{ByteOrder, LittleEndian};
use efi_variable_attributes::EfiVariableAttributes;
use uuid::Uuid;

pub struct EfiVariable {
    pub name: String,
    pub type_id: uuid::Uuid,
    pub attributes: EfiVariableAttributes,
    pub data: Vec<u8>,
    pub crc: u32
}

impl EfiVariable {
    pub fn create(record: Vec<u8>, name_size: usize, data_size: usize) -> EfiVariable {
        let mut name_bytes = vec![0u16; name_size / 2];
        LittleEndian::read_u16_into(&record[0..name_size], &mut name_bytes);
        
        return EfiVariable {
            name: String::from_utf16(&name_bytes).unwrap(),
            type_id: Uuid::from_slice(&record[name_size..(name_size + 16)]).unwrap(),
            attributes: EfiVariableAttributes::from_bits(LittleEndian::read_u32(&record[(name_size + 16)..(name_size + 24)])).unwrap(),
            data: record[(name_size + 20)..(name_size + 20 + data_size)].to_vec(),
            crc: LittleEndian::read_u32(&record[(name_size + 20 + data_size)..])
         };
    }
}