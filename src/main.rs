#[macro_use]
extern crate bitflags;
extern crate byteorder;
extern crate uuid;

mod efi_variable;
use std::io::Read;
use std::convert::TryInto;
use byteorder::{LittleEndian, ReadBytesExt};
use efi_variable::EfiVariable;

fn main() {
    let mut path = String::new();
    
    println!("Path: ");
    std::io::stdin().read_line(&mut path).unwrap_or_default();
    path = String::from(path.trim_end());

    println!("Reading {}...", path);

    let efi_vars = read_efi_vars_container(path);

    for efi_var in efi_vars {
        println!("Name:\t\t{}", efi_var.name);
        println!("Type:\t\t{}", efi_var.type_id);
        println!("Attributes:\t{:?}", efi_var.attributes);
        println!("Value:\t\t({} bytes)", efi_var.data.len());
        println!("CRC:\t\t{}", efi_var.crc);

        println!("");
    }
}

fn read_efi_vars_container(path: String) -> Vec<EfiVariable> {
    let mut result = Vec::new();

    let mut file = std::fs::File::open(path).unwrap();
    let length = file.metadata().unwrap().len();
    let mut position: u64 = 0;

    while position < length
    {
        let name_size = file.read_u32::<LittleEndian>().unwrap();
        let data_size = file.read_u32::<LittleEndian>().unwrap();
        position += 8;

        let record_size: u32 = name_size + data_size + /* EFI_GUID + (2 x u32) */ 24;
        let mut data = vec![0u8; record_size.try_into().unwrap()];
        file.read_exact(&mut data).unwrap();

        let record_length: u64 = record_size.try_into().unwrap();
        position += record_length;

        let record = EfiVariable::create(data, name_size.try_into().unwrap(), data_size.try_into().unwrap());
        result.push(record);
    }

    return result;
}